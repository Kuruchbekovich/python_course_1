# Напишите пример функции возвращающей значения с помощью return
Moi_dannye = [{
    "firma": "reebok",
    "tip": "krosovki",
    "size": 38
},
    {"firma": "reebok",
     "tip": "krosovki",
     "size": 39,
     },
    {"firma": "reebok",
     "tip": "krosovki",
     "size": 40,
     },
    {"firma": "reebok",
     "tip": "krosovki",
     "size": 41},
    {"firma": "adidas",
     "tip": "krosovki",
     "size": 38},
    {"firma": "adidas",
     "tip": "krosovki",
     "size": 39},
    {"firma": "adidas",
     "tip": "krosovki",
     "size": 40},
    {"firma": "nike",
     "tip": "krosovki",
     "size": 38, },
    {"firma": "nike",
     "tip": "krosovki",
     "size": 39, },
    {"firma": "nike",
     "tip": "krosovki",
     "size": 40, },
    {"firma": "nike",
     "tip": "krosovki",
     "size": 41, }
]


def filtr(odejda):
    sortirovka = []
    for botsy in odejda:
        if botsy["size"] < 39:
            botsy["status"] = "na sklade"
            sortirovka.append(botsy)

    return sortirovka


itog = filtr(Moi_dannye)

for sklad in itog:
    print(sklad)


# Напишите пример функции возвращающей значения с помощью return и принимающей в себя любое колличество аргументов

def sum(*args):
    formula = 0

    for summa in args:
        formula += summa

    return formula


resultat = sum(1, 2, 3, 4, 5, 6, 7, 10)

print(resultat)

# Напишите пример цикла в цикле

Moi_list = [["alma", "almurut", "persik", "jangak"], ["oruk", "danek", "sliva", "karagat"], ["kompot", "sok", "jem"]]

for frukty in Moi_list:
    for varenie in frukty:
        print(varenie)

# Напишите пример клуба с двумя функциями (функция охранник, функция администратор), функция охранника должна принимать в себя любое количество массивов с людьми

moi_club = [
    {"name": "Alina",
     "second_name": "Osmonkulova",
     "age": 28,
     "adres": "g.Bishkek",
     "gender": "female",
     "color": "black", },

    {"name": "Ailin",
     "second_name": "Nusupaeva",
     "age": 20,
     "adres": "g.Bishkek",
     "gender": "female",
     "color": "brown", },

    {"name": "Ilima",
     "second_name": "Mirlanova",
     "age": 18,
     "adres": "g.Bishkek",
     "gender": "female",
     "color": "black", },

    {"name": "Aida",
     "second_name": "Kanatova",
     "age": 25,
     "adres": "g.Bishkek",
     "gender": "female",
     "color": "blond", },

    {"name": "Altynai",
     "second_name": "Osmonkulova",
     "age": 17,
     "adres": "g.Bishkek",
     "gender": "female",
     "color": "blond", },
]
Moi_vtoroi_club = [
    {"name": "Ruslan",
     "age": 20,
     "gender": "male",
     "rost": 175},

    {"name": "Tilek",
     "age": 28,
     "gender": "male",
     "rost": 165},

    {"name": "Lapa",
     "age": 17,
     "gender": "male",
     "rost": 180},

    {"name": "Akyl",
     "age": 16,
     "gender": "male",
     "rost": 170},

    {"name": "Adilet",
     "age": 24,
     "gender": "male",
     "rost": 174},
]


def ohranik(*args):
    voshli = []
    for majory in args:
        for posititeli in majory:
            if posititeli["age"] >= 18:
                voshli.append(posititeli)
    return voshli


V_klub_voshli = ohranik(moi_club, Moi_vtoroi_club)
for vnutri in V_klub_voshli:
    print(vnutri)

print("----------------------------------------------------------------------------------------")


def adminstrator(*args):
    for itog in args:
        for prishli in itog:
            prishli["status"] = True


v_itoge = ohranik(moi_club, Moi_vtoroi_club)
adminstrator(v_itoge)

for proshli in v_itoge:
    print(proshli)
