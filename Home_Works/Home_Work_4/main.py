# пример использования циклов с массивом данных состоящих из цифр
moi_tsifry = [2, 4, 6, 8, 0, 10, 12, 14]
moi_tsifry2 = [1, 3, 5, 7, 9, 11, 13, 15]

for chetnye in moi_tsifry:
    print(chetnye)

for ne_chetnye in moi_tsifry2:
    print(ne_chetnye)

# пример использования циклов с массивом данных состоящих из словарей (dict)
# pervyi primer
moi_dict = [
    {"dc_hero": "flash"},
    {"dc_hero": "batman"},
    {"dc_hero": "superman"},
    {"dc_hero": "wonderwomen"},
    {"dc_hero": "kiborg"},
    {"dc_hero": "kid_flash"},
]
for hero in moi_dict:
    print(hero)

# vtoroi primer
moi_dict = [
    {"dc_hero": "flash"},
    {"dc_hero": "batman"},
    {"dc_hero": "superman"},
    {"dc_hero": "wonderwomen"},
    {"dc_hero": "kiborg"},
    {"dc_hero": "kid_flash"},
]

for hero_1 in moi_dict:
    print(hero_1["dc_hero"])


# пример  слово 'Horosho'

def primer():
    print("Horosho")


primer()


# Создайте простую функцию - складывающую 5 + 5

def summa():
    print(5 + 5)


summa()


# Создайте простую функцию - которая принимает в себя число и отнимает от принятого числа 5

def primer(itog):
    print(itog - 5)


primer(20)


# Создайте простую функцию - которая принимает в себя число и умножает его на 5
def primer_1(umnojit):
    print(umnojit * 5)


primer_1(100)


# Создайте простую функцию - которая принимает в себя число и вычисляет 15 процентов от принятого числа

def procent(v_itoge):
    print(v_itoge * 15 / 100)


procent(1000)


# Создайте простую функцию - которая принимает в себя строку и добавляет ей в конец слово 'hello'


def konec(slovo):
    print(slovo + "hello")


konec("Mirlan ")

# Создать свой клуб с использованием ммассива людей, и функции охранника для фильтрации людей

moi_club = [
    {"name": "Alina",
     "second_name": "Osmonkulova",
     "age": 28,
     "adres": "g.Bishkek",
     "gender": "female",
     "color": "black", },

    {"name": "Ailin",
     "second_name": "Nusupaeva",
     "age": 20,
     "adres": "g.Bishkek",
     "gender": "female",
     "color": "brown", },

    {"name": "Ilima",
     "second_name": "Mirlanova",
     "age": 18,
     "adres": "g.Bishkek",
     "gender": "female",
     "color": "black", },

    {"name": "Aida",
     "second_name": "Kanatova",
     "age": 25,
     "adres": "g.Bishkek",
     "gender": "female",
     "color": "blond", },

    {"name": "Altynai",
     "second_name": "Osmonkulova",
     "age": 17,
     "adres": "g.Bishkek",
     "gender": "female",
     "color": "blond", },
]

for kyzdar in moi_club:
    if kyzdar["age"] >= 18:
        print("-----prohodish-----", kyzdar["name"])
    kyzdar["otmetka"] = "proshla"
else:
    print("-----ne prohodish-----", kyzdar["name"])

for kyzdar in moi_club:
    print(kyzdar)
# Vtoroi primer s clubom

moi_vtoroi_club = [
    {"name": "Ruslan",
     "age": 30,
     "gender": "male",
     "rost": 175},

    {"name": "Tilek",
     "age": 33,
     "gender": "male",
     "rost": 165},

    {"name": "Lapa",
     "age": 32,
     "gender": "male",
     "rost": 180},

    {"name": "Akyl",
     "age": 30,
     "gender": "male",
     "rost": 170},

    {"name": "Adilet",
     "age": 31,
     "gender": "male",
     "rost": 174},
]
for kachalka in moi_vtoroi_club:
    if kachalka["rost"] <= 180:
        print("Proshel Jiraf", kachalka["name"])
