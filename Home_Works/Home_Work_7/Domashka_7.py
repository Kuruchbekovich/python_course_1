# Python Set add():

car_model_set = {"e200", "amg", "e700brabus"}
car_model_set.add("Galendvagen")
print(car_model_set)
print("-------------------------------------")

# Python Set remove():

imena = {"Arsen", "Mirlan", "Arlen", "Marlen"}
imena.remove("Mirlan")
print(imena)
print("-------------------------------------")

# Python Set copy():

phone_vendor = {"apple", "samsung", "huawei", "xiaomi"}
all_phone = phone_vendor.copy()

all_phone.add("oppo")

print("---Первый сет---", phone_vendor)
print("---скопированный сет---", all_phone)
print("--------------------------------------")

# Python Set clear()

bukvy = {'a', 'e', 'i', 'o', 'u'}
print('bukvy (до clear):', bukvy)

bukvy.clear()
print('bukvy(после clear):', bukvy)
print("--------------------------------------------")

# Python Set difference()

A = {'a', 'b', 'c', 'd'}
B = {'c', 'b', 'g'}

# Разница to A-B
print(A.difference(B))

# Разница to B-A
print(B.difference(A))
print("--------------------------------------------------")

# Python Set difference_update()

x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}

x.difference_update(y)

print(x)
print("-----------------------------------------------------------")

# Python Set discard()

x_set = {"acer", "lenovo", "hp"}
x_set.discard("lenovo")

print(x_set)
print("----------------------------------------------------------------")

# Python Set intersection()

prim_1 = {"a", "b", "c"}
prim_2 = {"c", "d", "e"}
prim_3 = {"f", "g", "c"}

result = prim_1.intersection(prim_2, prim_3)

print(result)
print("----------------------------")

# Python Set intersection_update()

A = {1, 2, 3, 4}
B = {2, 3, 4, 5, 6}
C = {4, 5, 6, 9, 10}

itog = A.intersection_update(B, C)

print('C =', C)
print('B =', B)
print('A =', A)
print("--------------------------------------------------------------")

# Python Set isdisjoint()

A_set = {"Bishkek", "Talas", "Karakol", "Osh"}
B_set = {"Naryn", "Jalalabad", "Batken"}
C_set = {"Naryn", "Tokmok"}

print(A_set.isdisjoint(B_set))
print(B_set.isdisjoint(C_set))
print("--------------------------------------------------------------")

# Python Set issubset()

Bihskek = {"Lenin", "Pervomai", "Oktyabr", "Sverdlov"}
Almaty = {"Lenin", "Pervomai", "Oktyabr", "Sverdlov", "Kenensary", "Alfarabi"}
Tokmok = {"Lenin", "Pervomai", "Oktyabr", "Sverdlov", "Manas"}
print(Bihskek.issubset(Almaty))
print(Almaty.issubset(Tokmok))
print("--------------------------------------------------------------")

# Python Set issuperset()

L = {"f", "e", "d", "c", "b", "a"}
M = {"a", "b", "c", "v"}

P = L.issuperset(M)

print(P)
print("-----------------------------------------------------------------------------------------------------")

# Python Set pop()

Raion = {"Lenin", "Pervomai", "Oktyabr", "Sverdlov", "Manas"}
print(Raion.pop())
print("--------------------------------------------------------------")

# Python Set symmetric_difference()

tsifry = {55, 66, 77, 88, 99, 111, }
tsifry_1 = {55, 66, 56, 45, 12, 11}
print(tsifry ^ tsifry_1)
print("--------------------------------------------------------------")

# Python Set symmetric_difference_update()

data = {555, 777, 999, 222, "Megacom"}
data_2 = {555, "Beeline", "Megacom", 222, 333}
data_3 = data.symmetric_difference_update(data_2)

print(data_3)  # Не создает новый сет
print(data)
print("--------------------------------------------------------------")

# Python Set union()

Z = {123, 456, "abc", "dfg"}
T = {789, 123, "asd", "lpl"}
D = Z.union(T)

print(D)
print("--------------------------------------------------------------")

# Python Set update()
set_number = {1, 2, 2, 4, 5, 6}
set_bukvy = {"a", "b", "c"}
set_number.update(set_bukvy)
print(set_number)

print("----------------------------------------------------------------")
# Написать цикл с break

obiekt = 0

for obiekt in range(10):
    if obiekt == 5:
        break  # break here

    print('Number is ' + str(obiekt))

print("--------------------------------------")

# Написать цикл с continue
obiekt_2 = 0

for obiekt_2 in range(10):
    if obiekt_2 == 5:
        continue

    print('Number is ' + str(obiekt_2))
