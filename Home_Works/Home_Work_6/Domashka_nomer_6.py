# Напишите пример содержащий функцию принимающую *args

def name(*args):
    print("Его зовут" + args[2])


name("Mirlan", "Akyl", " Ruslan")


# Напишите пример содержащий функцию принимающую *kwargs

def raion(**kwargs):
    print("Он живет в" + kwargs["pervyi"])


raion(pervyi=" Djale ", vtoroi=" Asanbae")


# Напишите пример содержащий функцию принимающую *args и *kwargs

def sostav(*args, **kwargs):
    print("---args---", args)
    print("---kwargs---", kwargs)


sostav(55, 77, 555, 777, a="MIR", b="VIP", c="ONE")


# Напишите пример содержащий функцию принимающую *args и *kwargs а также простые агрументы
def primer(*args, **kwargs):
    print("---prishlo---", args)
    print("---prishlo imenuemye---", kwargs)


primer(100, 200, 300, 400, 500, imia="Kalch", familia="kalchov", otchestvo="kalchovich", )

# Напишите пример lambda функции которая возвращает результат умножения числа пришедшего в аргументы на 5

file = lambda tsifra: tsifra * 5

itog = file(100)

print(itog)

# Напишите пример lambda функции которая возвращает результат деления числа пришедшего в аргументы на 5

file_2 = lambda delenie: delenie / 5

itog_2 = file_2(500)

print(int(itog_2))

# Напишите пример lambda функции которая возвращает значение 'age' из пришедшего в аргументы словаря

my_dict = [
    {"nick": "Almaz",
     "age": 25},
    {"nick": "Aida",
     "age": 30},

]
my_dict.sort(key=lambda spisok: spisok["age"])
for fatality in my_dict:
    print(fatality)

# Напишите пример создания list
My_list = ["Kuhnia", "Zal", "Spalnaia"]

# Напишите чтения из list
print(My_list)

# Напишите пример изменения list
My_list[1] = "Detskaia"
print(My_list)

# Напишите пример добавления в list
My_list.append("Ubornaia")
print(My_list)

# Напишите пример удаления из list
My_list.pop(0)
print(My_list)

# Напишите пример перевода list в tuple
dom = ["kliuch", "dver", "okno"]

thistuple_1 = tuple(dom)
print(thistuple_1)

# Напишите пример создания tuple
thistuple = ("apple", "banana", "cherry", "apple", "cherry")

# Напишите чтения из tuple
print(thistuple)

# Напишите пример перевода tuple в list:

my_tuple = ("Mers", "BMW", "Toyota", "apple", "Honda")

my_list_1 = list(my_tuple)

print(my_tuple)
print(my_list_1)

# Напишите пример создания dict:

semia = {
    "papa": "prezident",
    "mama": "premier ministr",
    "deti": "narod",
}
# Напишите чтения из dict:

print(semia)
print("--------------------------------------------------")
# Напишите пример изменения dict

semia["deti"] = "schastie"
print(semia)
# Напишите пример добавления в dict:

semia["gosti"] = "deputaty"
print(semia)

# Напишите пример удаления из dict:

semia.pop("papa")
print(semia)


# Напишите пример применения lambda функции в методе list (sort):
Moia_baza = [{
    "firma": "reebok",
    "tip": "krosovki",
    "size": 38
},
    {"firma": "reebok",
     "tip": "krosovki",
     "size": 39,
     },
    {"firma": "reebok",
     "tip": "krosovki",
     "size": 40,
     },
    {"firma": "reebok",
     "tip": "krosovki",
     "size": 41}
]

Moia_baza.sort(key=lambda razmer: razmer["size"])
for obuv in Moia_baza:
    print(obuv)

# Почитате про python sorted: Прочитал

# Напишите пример применения lambda функции в методе list (sorted)

records = [{"name": "Андрей", "age": 20, "score": 90}, {"name": "Игорь", "age": 16, "score": 75}, {"name": "Артем", "age": 23, "score": 99}]

sorted(records, key=lambda recordy: recordy["score"])
for ochko in records:
    print(ochko)


# Напишите пример создания set

this_set = {"Bishkek", "Bishkek", "Osh", "Talas"}

print(this_set)